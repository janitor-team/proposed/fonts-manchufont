(!!! Do not use this document. Refer to FONTLOG.txt instead)

Manchu Font v2.006 Draft

Warning:
This package is internal development release, means it contains bugs. Features, quality and other user experiences not represent as they are in the final release.
Not recommended for general public use.


Features:
Manchu Font is a free TrueType favor OpenType font for Mac and PC. It contains Manchu characters in Unicode range start from #1800.

This package also includes free ManjuKey for Windows application, a preliminary method for entering Manchu letters using keyboard. Refer to the UserGuide.Doc for detail instructions.

Files Included:
GNU Free Documentation License.txt (License agreement for Manchu Font documentations) 
GNU GENERAL PUBLIC LICENSE.txt (License agreement for Manchu Font)
Manchu Font.ttf (TrueType font for Manchu)
Manchu Font.vfb (FontLab source code for Manchu Font)
Manchu FontStandard(Draft).doc (Document for Manchu Font)
ReadMe.txt (This file)

.\ManjuKey\ (ManjuKey input method files)
	ManjuKey.msg (Installation file for ManjuKey)
	ManjuKey UserGuid(Draft).doc (Document for ManjuKey)
	ManjuKey.klc (Microsoft Keyboard Layout source code for ManjuKey)


Installation:

To install the font, just copy the Manchu Font.ttf file into your Control Panel/Fonts folder.

Upgrade:
If you already have a previous Manchu font installed, you need to uninstall it from Control Panel/Fonts folder first. 

Uninstall:
To uninstall the font, delete Manchu font from your Control Panel/Fonts folder. This is also required when you upgrade the font from a previous testing version.


License:
The original font library was created by Manchu Group(http://groups.msn.com/manchu). Its released to public domain under GNU Public License(www.gnu.org).

In sample English, feel free to download, use, redistribute, modify, add, comment and, most important, contribute to this free font library.
Feel free to claim your works and respect other people's.


Copyright (c) 2005-2007 by Manchu Group. Please contact Manchu Group regarding copyright issues.